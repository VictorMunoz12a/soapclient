package com.jaom.ws.soap;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import com.jaom.ws.trainings.CustomerOrdersPortType;
import com.jaom.ws.trainings.GetOrdersRequest;
import com.jaom.ws.trainings.GetOrdersResponse;
import com.jaom.ws.trainings.Order;
import com.vamp.ws.soap.CustomerOrdersWsImplService;

public class CustomerOrderWsClient {
	public static void main( String args[]) throws MalformedURLException{
		CustomerOrdersWsImplService service= new CustomerOrdersWsImplService(new URL("http://localhost:8082/webservice/customerorderservice?wsdl"));
		CustomerOrdersPortType customerOrdersWsImplPort=service.getCustomerOrdersWsImplPort();
		
		GetOrdersRequest request= new GetOrdersRequest();
		request.setCustomerId(BigInteger.valueOf(1));
		
		GetOrdersResponse response= customerOrdersWsImplPort.getOrders(request);
		List<Order> orders=response.getOrder();
		
	

		System.out.println("Number of orders for the customer are: "+orders.size());
		
		for (Order p: orders) {
			System.out.println(p.getProduct().get(0).getDescription());
		}
	}
}
